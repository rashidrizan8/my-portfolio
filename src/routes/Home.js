import React from 'react'
import Navbar from '../Component/Navbar'
import Heroimg from "../Component/Heroimg"

const Home = () => {
  return (
    <div>
      <Navbar />
      <Heroimg />
    </div>
  )
}

export default Home
